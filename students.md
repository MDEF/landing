---
title: Students
layout: page
---

<a href="https://mdef.gitlab.io/2018/adriana.tamargo/">Adriana Tamargo Iturri</a><br />
<a href="https://mdef.gitlab.io/jessica.guy/">Jessica Guy</a><br />
<a href="https://mdef.gitlab.io/2018/alexandre.acsensi/">Alexandre Acsensi Valiente</a><br />
<a href="https://mdef.gitlab.io/2018/nicolas.viollier/">Nicolás Viollier</a><br />
<a href="https://mdef.gitlab.io/2018/thomas.barnes/">Thomas Barnes</a><br />
<a href="https://mdef.gitlab.io/2018/julia.danae/">Julia Danae Bertolaso</a><br />
<a href="https://mdef.gitlab.io/2018/ola.lukaszewska/">Aleksandra Łukaszewska</a><br />
<a href="https://mdef.gitlab.io/2018/gabor.mandoki/">Gábor Lászlo Mándoki</a><br />
<a href="https://mdef.gitlab.io/2018/julia.quiroga/">Julia Quiroga</a><br />
<a href="https://mdef.gitlab.io/2018/maite.villar/">Maite Villar Latasa</a><br />
<a href="https://mdef.gitlab.io/2018/ilja.panic/">Ilja Aleksandar Panić</a><br />
<a href="https://mdef.gitlab.io/2018/saira.raza/">Saira Raza</a><br />
<a href="https://mdef.gitlab.io/2018/emily.whyman/">Emily Whyman</a><br />
<a href="https://mdef.gitlab.io/2018/silvia.ferrari/">Silvia Matilde Ferrari Boneschi</a><br />
<a href="https://mdef.gitlab.io/2018/veronica.tran/">Nhu Tram Veronica Tran</a><br />
<a href="https://mdef.gitlab.io/2018/gabriela.martinez/">Gabriela Martinez Pinheiro</a><br />
<a href="https://mdef.gitlab.io/2018/oliver.juggins/">Oliver Juggins</a><br />
<a href="https://mdef.gitlab.io/2018/rutvij.pathak/">Rutvij Pathak</a><br />
<a href="https://mdef.gitlab.io/2018/fifa.jonsdottir/">Fífa Jónsdóttir</a><br />
<a href="https://mdef.gitlab.io/2018/ryota.kamio/">Ryota Kamio</a><br />
<a href="https://mdef.gitlab.io/2018/vasiliki.simitopoulou/">Vasiliki Simitopoulou</a><br />
<a href="https://mdef.gitlab.io/2018/barbara.drozdek/">Barbara Drozdek</a><br />
<a href="https://mdef.gitlab.io/2018/katherine.vegas/">Katherine Stephania Vegas Garcia</a><br />
<a href="https://mdef.gitlab.io/2018/laura.alvarez/">Laura Álvarez Florez</a><br />
<a href="https://mdef.gitlab.io/2018/vesa.gashi/">Vesa Gashi</a><br />
