---
title: Styleguide
layout: page
---

<h1>h1 heading</h1>
<h2>h2 heading</h2>
<h3>h3 heading</h3>
<h4>h4 heading</h4>
<h5>h5 heading</h5>
<h6>h6 heading</h6>
<p>p pararaph</p>
<p> <small>small paragraph</small> </p>
<p> <strong>strong paragraph</strong> </p>


<section>

  <p>Buttons from theme. See `_basic.scss` </p>

  <a class="button" href="#">button</a>
  <a class="button button--light" href="#">light</a>
  <a class="button button--large" href="#">large</a>
  <a class="button button--overlay" href="#">overlay</a>

  <h3>Links</h3>
  <a href="http://www.url.com" target="_blank">Anchor Text</a>

</section>
