---
title: Design Studio
layout: "page"
order: 1
---

![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/d829960529ef-IMG_0308.jpg)

### Faculty
Tomas Diez  
Oscar Tomico

### Assistant
Mariana Quintero    
Lucas Peña, Ingi Freyr (Exhibition curators) 

### Syllabus and Learning Objectives
MDEF Research, Design and Development studios aim to take research areas of interest and initial project ideas into an advanced concretion point, and execution plan. The studio structure in three terms could be understood as follows:

TERM 1: Research Studio: Analyzing the past. References, state of the art. Identifying areas of interest.

TERM 2: Design Studio: Forming the present. Building the foundations. Applying knowledge into practice. Prototyping and experimenting.

TERM 3: Development Studio: Defining the future. Establishing roadmaps. Forming partnerships. Testing ideas and prototypes in the real world.  

The Design Studio time will be dedicated to supporting students to focus their work on the development of their design intervention or project. During the studio, studio leaders will bring invited guests to introduce topics of interest to the process and to participate in tutorials during the desk crits.  

### Total Duration
Weekly studio days will be every Tuesday from 11hrs to 19hrs. The program will evolve to fit the progress of the students. A general outline will be the following:  
11hrs - 13hrs Work on the final exhibition.  
13hrs - 15hrs Pitch over lunch. Lunch brought by each student.  
15hrs - 19hrs Desk crits, tutorials.  

### Structure and Phases
* *__Week 1: Apr 23__*     
15hrs - 16hrs Chiara and Mariana present the 3rd term (general structure of the term, Studio structure)    
16hrs - 18hrs Class work: students present their project planning   
Student’s deliverables: Project Planning  
* *__Week 2: Apr 30__*    
10hrs - 13hrs Desk crits  
13hrs - 15hrs Carlos B. Steinblock presents his work (blockchain)   
Student’s deliverables: Context / Area of Interest + State of the Art + Description of the Intervention    
* *__Week 3: May 7__*    
11hrs - 13hrs Work on the final exhibition (Luke + Ingi + Mariana)  
13hrs - 15hrs Masterclass on exhibition curation  
15hrs - 19hrs Desk crits  
Student’s deliverables: Projection of the intervention in the future    
* *__Week 4: May 14__*    
13hrs - 19hrs - MIDTERM REVIEWS.  
* *__Week 5: May 21__*    
11hrs - 13hrs Work on the final exhibition (Luke + Ingi + Mariana)  
13hrs - 15hrs Pitch over lunch  
15hrs - 19hrs Desk crits  
Student’s deliverables: Document your intervention's design actions and results.    
* *__Week 6: May 28__*    
11hrs - 13hrs Work on the final exhibition (Luke + Ingi + Mariana)  
13hrs - 15hrs Pitch over lunch  
15hrs - 19hrs Desk crits  
Student’s deliverables: Sustainability model    
* *__Week 7: June 4__* 
13hrs - 19hrs Tutorials - Final Show Production   
Student’s deliverables: Final reflection / Learnings    
* *__Week 8: June 11__* 
13hrs - 19hrs Tutorials - Final Show Production    

### Output
Deliverables:  
Document (2-4 pages per chapter, 7 chapters)   
Set of 5 photos to communicate the project (high / print quality)    
Video (2-3 mins max)    
Prototype / Platform / etc.   
Exhibition production  
Presentation

### Grading Method
Academic level of the final document (framing of the opportunity, state of the art, literature review, ...) (20%)  
Level of clarity and detail of the communication material (photos, video, exhibition set-up)(20%)  
Quality of the designed prototype/platform/ etc (20%)  
Social value of the interventions based on the analysis of the results (20%)  
Depth in the personal reflection and main learning points (10%)  
Motivation level, proactive behaviors & attendance (10%)

[Studio structure and deliverables](https://docs.google.com/presentation/d/1uwZmQCS684JTcRjWwPDtZLbllfg_N52pCyQlU993TIE/edit?usp=sharing) 

### Bibliography
[Speculative Everything	- Anthony Dunne and Fiona Raby](https://mitpress.mit.edu/books/speculative-everything)  
[Adversarial Design	- Carl DiSalvo](https://mitpress.mit.edu/books/adversarial-design)  
[Massive Change - Bruce Mau, Jennifer Leonard and Institute without Boundaries](http://www.brucemaudesign.com/work/massive-change)  
[Design for the Real World: Human Ecology and Social Change	- Victor Papanek](https://www.amazon.com/Design-%C2%ADReal-%C2%ADWorld-%C2%ADEcology-%C2%ADSocial/dp/0897331532)  
[Liquid Modernity - Zygmunt Bauman](https://www.amazon.com/Liquid-%C2%ADModernity-%C2%ADZygmunt-%C2%ADBauman/dp/0745624103)  
[Who Owns the Future? - Jason Lanier](https://www.amazon.es/gp/product/B008J2AEY8/ref=dp-%C2%ADkindle-%C2%ADredirect?ie=UTF8&btkr=1)  
[This Changes Everything - Naomi Klein](https://thischangeseverything.org/book/)  
[To Save Everything, Click Here: The Folly of Technological Solutionism - Evgeny Morozov](https://www.amazon.com/gp/product/1610393708?psc=1&redirect=true&ref_=oh_aui_detailpage_o01_s00)  
[Democratizing Innovation - Eric Von Hippel](https://www.amazon.com/gp/product/0262720477?psc=1&redirect=true&ref_=oh_aui_detailpage_o01_s01)  
[Cradle to Cradle: Remaking the Way We Make Things - Michael Braungart, William McDonough](http://www.amazon.com/gp/product/0865475873?psc=1&redirect=true&ref_=od_aui_detailpages00)  
[Macrowikinomics: New Solutions for a Connected Planet - Don Tapscott, Anthony D. Williams](http://www.amazon.com/gp/product/1591844282?psc=1&redirect=true&ref_=od_aui_detailpages00)  
[The Third Industrial Revolution: How Lateral Power Is Transforming Energy, the Economy, and the World - Jeremy Rifkin](http://www.amazon.com/gp/product/0230115217?psc=1&redirect=true&ref_=od_aui_detailpages00g)  
[The Death and Life of Great American Cities - Jane Jacobs](https://www.amazon.com/Death-Life-Great-American-Cities/dp/067974195X)    
[The Third Plate - Dan Barber](http://www.thethirdplate.com/)  
[Free Innovation - Eric Von Hippel](https://www.amazon.com/Free-Innovation-Press-Eric-Hippel/dp/0262035219)  
[Limits to Growths - Donella H. Meadows](https://www.amazon.com/Limits-Growth-Donella-H-Meadows/dp/193149858X)  
[The Human Face of Big Data - Rick Smolan](https://www.amazon.com/Human-Face-Big-Data/dp/1454908270/ref=sr_1_1?s=books&ie=UTF8&qid=1509138845&sr=1-1&keywords=the+human+face+of+big+data)   

### TOMAS DIEZ
![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/0302393192f2-Screenshot_2018_09_10_at_12.37.01_20180912191624503.jpg)

### Email Address
<tomasdiez@iaac.net>

### Personal Website
[Tomas Diez](http://tomasdiez.com)

### Twitter Account
@tomasdiez

### Facebook Profile
tomasdiez77

### Professional BIO
Tomas Diez is a Venezuelan Urbanist specialized in digital fabrication and its implications on the future cities and society. He is the co-founder of Fab Lab Barcelona, leads the Fab City Research Laboratory, and is a founding partner of the Fab City Global Initiative. He is the director of the Master in Design for Emergent Futures at the Institute for Advanced Architecture of Catalonia (IAAC) in Barcelona, where he is faculty in urban design and digital fabrication. Tomas is co-founder of other initiatives such as Smart Citizen (open source tools for citizen engagement), Fab City (locally productive, globally connected cities), Fablabs.io (the listing of fab labs in the world), and StudioP52 (art and design space in Barcelona).

### OSCAR TOMICO
![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/6cf613d0f7fa-IMG_9099_Edit_S.jpg)

### Email Address
<otomico@elisava.net>

### Personal Website
[Oscar Tomico - Elisava](http://www.elisava.net/en/center/professorate/oscar-tomico-plasencia)

### Twitter Account
@otomico

### Professional BIO
Oscar Tomico holds an MSc degree in Industrial Engineering from Polytechnic University of Catalonia (Spain) and a PhD from the same institution, awarded in 2007 with Cum Laude. During his research into Innovation Processes in Product Design, he investigated subjective experience-gathering techniques based on constructivist psychology. After finishing his PhD he worked as a consultant for Telefonica R&D (Barcelona). Tomico joined Eindhoven University of Technology (TU/e) in 2007 as Assistant Professor. He has been a guest researcher and lecturer at AUT Creative technologies (New Zealand), at TaiwanTech (Taiwan), Swedish School of Textiles (Sweden), Institute of Advanced Architecture (Spain), University of Tsukuba, Aalto (Finland) to name a few. During his sabbatical in 2015 he worked as a consultant for the functional textiles department at EURECAT (Spain). He recently (2017) became the head of the Industrial Design Bachelor’s degree program at ELISAVA University School of Design and Engineering of Barcelona.