---
title: Engaging Narratives
layout: "page"
order: 9
---

![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/AliceRabbitHole_S.jpg)  

### Faculty
Heather Corcoran  
Bjarke Calvin  
Tomas Diez  
Oscar Tomico  

### Assistant
Mariana Quintero  

### Syllabus and Learning Objectives
How to structure the story about your journey in the Master in Design for Emergent Futures. MDEF’s term 1 is structured in intense weeks where faculty and students learn, debate, and explore new tools, methods, theories and practices in design education, research and innovation. This week is intended to make sense of the knowledge acquired during the first term of MDEF, and articulate it as a series of chapters (weeks), of a whole book (term).  

### Presentations from Tutors  

Heather Corcoran:  

<iframe src="https://drive.google.com/file/d/1bnCl2zFVtdaW31PBkuBdzzCxUeYiPBOf/preview" width="640" height="480"></iframe>  

Bjarke Calvin:  

<iframe src="https://drive.google.com/file/d/1UdzNjFSw13t1EFyYD0GyU4KMWMDZQxM-/preview" width="640" height="480"></iframe>  

### Some Insights created by Students   
[MDEF Insights](http://duckling.me/interest/emergentfutures)   

### Total Duration  
Classes: 3-6 p.m, Mon-Fri (15 per week)  
Student work hours: 20 hours  
Total hours per student: 35 hours  

### Structure and Phases
* *Day 1: Group debate and individual discussions*     
Refine your interest area, position yourself in the existing work, select the tools you will use, introduce your work-plan and documentation/distribution strategy. Articulate a narrative between these elements of your journey with a specific target audience in mind.  
* *Day 2: Practical Narratives Workshop with Kickstarter*    
The construction of thoughtful, compelling narratives should extend beyond the creative aspects of your practice. “Practical narratives” – the kinds of stories you tell in grant writing, fundraising, business proposals, pitches and more – can be some of the most powerful communications in your career. In this session, we’ll look at these kinds of narratives as an opportunity to invite your audience to get involved in your creative process, using the Kickstarter format as a new archetype to learn from. 
Workshop mini-structure:  
Context and analysis   
After an introductory lecture, we'll look at good examples of project proposals, drawn from Kickstarter and other sources – and analyse them together. Why are they compelling? What are they asking for? How are they inviting a crowd or community into their process?   
Exercise 1 (in-class): Title, Subtitle & Lead Image   
Using a project idea of your own, sketch out a compelling title, subtitle, and lead image for your concept   
Exercise 2 (take home): Outline or Storyboard   
Develop this first exercise into a proposal outline, or storyboard    
* *Day 3: Presentation and discussion*    
Students present & discuss Exercise 2   
One-on-one sessions   
Time for follow-up and individual feedback on students' own practical narratives   
* *Day 4: Tools for new engaging narratives*    
Duckling argues that social media in its DNA is built to divide us because it's business model is based on micro-segmentation. So it’s unlikely that social media will ever become beneficial for us. We have to build an entirely new media category instead.  
We call this next media category “Insight media,” and we built it on contextual and collective human thinking rather than personal promotion. We have made an app for Insight Media, which is called Duckling. Through Duckling, people create stories about their key insights, that are passed on, changed and expanded by the Duckling network. The result is ideas, insights, and inspiration that deepen our understanding of ourselves and the world.   
Mini-Workshop structure:
1. General introduction to why documentary storytelling makes sense, why exactly now, and why in the Fablab context
2. A bit of hands-on theory on how to construct a good documentary story
3. Demo of Duckling and a small assignment
4. Briefing on doing small stories the rest of the semester, and what it will generate for the students and for all of us.
* *Day 5: Student Showcase*    
Prepare a showcase of your narratives using computers and other types of screens in the classroom.  

### Output
One page text explaining your area of interest, and plans to execute it during the year. A short video (story) using social medial tools given in the class. 

### Grading Method  
Documentation: 30%  
Presentation: 70%  

### Bibliography
To be announced during the class  

### Requirements for the Students
Pre-install [Duckling](https://duckling.co) in iOS device  

### Infrastructure Needs
Classroom, projector, iOS devices.  

### HEATHER CORCORAN  

### Affiliation 
Kickstarter  

### Personal Website  
[Heather Corcoran](http://kickstarter.com)  

### Twitter Account
@kickstarter 

### Professional BIO
Heather Corcoran is Outreach Lead at the creative funding platform Kickstarter. Based in London, she works closely with artists, innovators, creators, and makers across Europe who use Kickstarter to bring new projects to life. Before that, she was the Executive Director of the digital art nonprofit Rhizome, based at the New Museum, New York. 

### BJARKE CALVIN 

### Affiliation 
Duckling   

### Personal Website  
[Bjarke Calvin](http://www.duckling.co)  

### Professional BIO
Bjarke Calvin is a CEO, Entrepreneur and Advisor: He creates projects that empower people socially with storytelling. His main project is Duckling.