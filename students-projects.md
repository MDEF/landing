---
title: Students Projects
layout: page
---

<!-- DO NOT CHANGE THIS! Instead edit the file /_data/student-projects -->
{% assign sorted = site.data.student-projects | sort: 'image' %}
{% for project in sorted %}
<div class="portfolio-item">
  <a class="portfolio-item__link" href="{{ project.url }}">
    <div class="portfolio-item__image">
      <img src="{{ project.image }}" alt="{{ project.name }}">
    </div>

    <div class="portfolio-item__content">
      <div class="portfolio-item__info">
        <h2 class="portfolio-item__title">{{ project.name }}</h2>
        <p class="portfolio-item__subtitle">{{ project.url }}</p>
      </div>
    </div>
  </a>
</div>
{% endfor %}
